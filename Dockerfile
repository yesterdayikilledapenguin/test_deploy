FROM node:10 as build

WORKDIR /app

COPY package.json .
RUN npm install

COPY . .
RUN npm run-script build


FROM nginx:1.19.4-alpine
COPY --from=build /app/static/ /usr/share/nginx/html/static/
COPY --from=build /app/build/ /usr/share/nginx/html/

